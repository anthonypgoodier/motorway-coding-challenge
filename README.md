# Motorway UI Test
### 1. UI development

Developed and tested in Desktop Chrome

### 2. Performance

The API that is returning images is rather slow. Show how it can be sped up, and show how you would measure the improvement in performance.

- Use a CDN
- Serve up thumbnail sized images to use in the gallery mode
- Ensure images are compressed
- Set appropriate caching on API & images
- Measure size of images being requested from S3, these should significantly reduce size and cost

### 3. Forms

Toggle the form using the floating button in the bottom left.
Form console logs out submit.