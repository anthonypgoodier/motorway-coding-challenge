import React from 'react'

export const Loader = () => {
  return (
    <div className="loader">
      <svg className="loader__icon" x="0px" y="0px"
        viewBox="0 0 100 100">
        <circle fill="none" stroke="#fff" strokeWidth="4" cx="50" cy="50" r="44" style={{opacity: '0.5'}}/>
        <circle fill="#fff" stroke="#ddd" strokeWidth="3" cx="8" cy="54" r="6" >
          <animateTransform
            attributeName="transform"
            dur="2s"
            type="rotate"
            from="0 50 48"
            to="360 50 52"
            repeatCount="indefinite" />
        </circle>
      </svg>
    </div>
  )
}
