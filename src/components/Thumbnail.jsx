import React from 'react'

export const Thumbnail = ({img, index, imageClickHandler, imageLoaded, showImg}) => {

  const duration = index * .15;
  const animationDuration = duration.toFixed(2)

  const handleKeyboard = (e) => {
    if (e.key === 'Enter') {
      imageClickHandler(img)
    }
  }

  return (
    <picture className={`gallery__img-container ${ showImg ? 'gallery__img-container--animate' : ''}`} key={img.id} onKeyDown={handleKeyboard} onClick={() => imageClickHandler(img)} style={{animationDelay: `${animationDuration}s`}} onLoad={imageLoaded} tabIndex="0">
      {/* <source srcSet={`${img.url}.webp`} type="image/webp" /> */}
      {/* I have hidden the webp version as this was causing serious slowdown in chrome when rendering and did not have enough time left to investigate */}
      <source srcSet={`${img.url}.jpg`} type="image/jpeg" /> 
      <img className="gallery__img" src={`${img.url}.jpg`} alt={img.description}/>
    </picture>
  )
}
