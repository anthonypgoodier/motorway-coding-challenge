import React, { useEffect, useRef } from 'react'

export const Modal = ({img, showModal, setShowModal}) => {
  const closeButton = useRef(null);

  useEffect(() => {
    if (closeButton.current) {
      closeButton.current.focus();
    }
  }, [showModal]);

  if ( !showModal ) return null;

  const handleKeyboard = (e) => {
    if (e.key === 'Enter') {
      setShowModal(false)
    }
  }

  return (
    <div className="modal">
      <svg ref={closeButton} className="modal__close" onKeyDown={handleKeyboard} onClick={() => setShowModal(false)} viewBox="0 0 32 30" x="0px" y="0px" tabIndex="0"><title>Close Modal</title><g stroke="none" strokeWidth="1" fillRule="evenodd"><path d="M17.8749299,15.9607163 L20.0355339,18.1213203 C20.4260582,18.5118446 20.4260582,19.1450096 20.0355339,19.5355339 C19.6450096,19.9260582 19.0118446,19.9260582 18.6213203,19.5355339 L16.4607163,17.3749299 L14.3393959,19.4962502 C13.9488717,19.8867745 13.3157067,19.8867745 12.9251824,19.4962502 C12.5346581,19.1057259 12.5346581,18.4725609 12.9251824,18.0820366 L15.0465027,15.9607163 L12.9644661,13.8786797 C12.5739418,13.4881554 12.5739418,12.8549904 12.9644661,12.4644661 C13.3549904,12.0739418 13.9881554,12.0739418 14.3786797,12.4644661 L16.4607163,14.5465027 L18.5820366,12.4251824 C18.9725609,12.0346581 19.6057259,12.0346581 19.9962502,12.4251824 C20.3867745,12.8157067 20.3867745,13.4488717 19.9962502,13.8393959 L17.8749299,15.9607163 Z M16.5,26.5 C10.7010101,26.5 6,21.7989899 6,16 C6,10.2010101 10.7010101,5.5 16.5,5.5 C22.2989899,5.5 27,10.2010101 27,16 C27,21.7989899 22.2989899,26.5 16.5,26.5 Z M16.5,24.5 C21.1944204,24.5 25,20.6944204 25,16 C25,11.3055796 21.1944204,7.5 16.5,7.5 C11.8055796,7.5 8,11.3055796 8,16 C8,20.6944204 11.8055796,24.5 16.5,24.5 Z"/></g></svg>
      <picture className="modal__img-container" key={img.id} style={{backgroundColor: img.color}}>
        <source srcSet={`${img.url}.webp`} type="image/webp" />
        <source srcSet={`${img.url}.jpg`} type="image/jpeg" /> 
        <img className="modal__img" src={`${img.url}.jpg`} alt={img.description} />
      </picture>
    </div>
  )
}
