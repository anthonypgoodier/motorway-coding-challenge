import React, { useState } from 'react'

export const Form = () => {

  const [firstName, setFirstName] = useState("");
  const [email, setEmail] = useState("");
  const [DOB, setDOB] = useState('2021-01-01');
  const [favColour, setFavColour] = useState("");
  const [salary, setSalary] = useState(0);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log( 'SUBMITTING FORM: ', {
      firstName,
      email,
      DOB,
      favColour,
      salary
    })
  }

  return (
    <form className="form" onSubmit={handleSubmit}>
      <label className="form__label" htmlFor="email">
        First Name
        <input
          className="form__input"
          type="text"
          id="firstName"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </label>
      <label className="form__label" htmlFor="email">
        Email
        <input
          className="form__input"
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </label>
      <label className="form__label" htmlFor="date">
        Date of Birth
        <input
          className="form__input"
          type="date"
          id="date"
          value={DOB}
          onChange={(e) => setDOB(e.target.value)}
          required
        />
      </label>
      <label className="form__label" htmlFor="favColour">
        Favourite Colour
        <input
          className="form__input"
          type="text"
          id="favColour"
          value={favColour}
          onChange={(e) => setFavColour(e.target.value)}
          required
        />
      </label>
      <label className="form__label" htmlFor="email">
        Salary (£0 to £1,000,000)
        <div className="form__salary">Current: £{salary}</div>
        <input
          className="form__input"
          type="range"
          min="0"
          max="1000000"
          step="1000"
          id="salary"
          value={salary}
          onChange={(e) => setSalary(e.target.value)}
        />
      </label>
      <button className="form__submit" type="submit">Submit</button>
    </form>
  )
}